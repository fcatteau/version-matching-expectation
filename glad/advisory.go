package glad

import (
	"os"
	"path/filepath"

	"github.com/bmatcuk/doublestar/v4"
	"gopkg.in/yaml.v2"
)

// Advisory is a security advisory published on the GitLab Advisory Database (GLAD).
type Advisory struct {
	AffectedRange string   `yaml:"affected_range"`
	FixedVersions []string `yaml:"fixed_versions"`
	Package       Package  `yaml:"package_slug"`
}

// Advisory reads an advisory from a file.
func ReadAdvisory(path string) (*Advisory, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	a := Advisory{}
	err = yaml.NewDecoder(f).Decode(&a)
	return &a, err
}

// Advisory reads advisories from a directory.
// It assumes that any name that ends with ".yml"
// is a YAML file that validates the schema defined in
// https://gitlab.com/gitlab-org/security-products/gemnasium-db#yaml-schema.
func ReadAdvisories(root string) ([]Advisory, error) {
	matches, err := doublestar.FilepathGlob(filepath.Join(root, "**/*.yml"))
	if err != nil {
		return nil, err
	}
	aa := []Advisory{}
	for _, path := range matches {
		a, err := ReadAdvisory(path)
		if err != nil {
			return nil, err
		}
		aa = append(aa, *a)
	}
	return aa, nil
}
