package glad

import (
	"strings"
)

// Package is an affected package referenced by the GitLab Advisory Database (GLAD).
type Package struct {
	Type string
	Name string
}

// UnmarshalYAML decodes a package slug.
func (p *Package) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var slug string
	if err := unmarshal(&slug); err != nil {
		return err
	}

	parts := strings.SplitN(slug, "/", 2)
	if len(parts) > 1 {
		p.Name = parts[1]
	}
	p.Type = parts[0]

	return nil
}
