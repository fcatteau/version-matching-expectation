package main

import (
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/vrange"
	"gitlab.com/gitlab-org/security-products/version-matching-expectation/glad"

	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/vrange/gem"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/vrange/golang"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/vrange/npm"
	// _ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/vrange/nuget"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/vrange/php"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v4/vrange/semver"
)

const (
	flagLicensesV1Path = "licenses-export"
)

func main() {
	if err := makeApp().Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

// Expectation combines package information, a version range,
// and versions that are in and out of the range.
type Expectation struct {
	Package      Package  `json:"package"`
	Versions     Versions `json:"versions"`
	VersionRange string   `json:"range"`
}

type Package struct {
	Type string `json:"type"`
	Name string `json:"name"`
}

type Versions struct {
	In  []string `json:"in"`
	Out []string `json:"out"`
	Err []string `json:"err,omitempty"`
}

func makeApp() *cli.App {
	flags := []cli.Flag{
		&cli.StringFlag{
			Name:    flagLicensesV1Path,
			Usage:   "Root directory of licenses export v1 (CSV files)",
			EnvVars: []string{"LICENSES_V1_PATH"},
		},
	}
	flags = append(flags, vrange.Flags()...)

	return &cli.App{
		Name:      "generate",
		Usage:     "Generate expectations from files or directories of the GitLab Advisory DB",
		ArgsUsage: "[path] ...",
		Flags:     flags,
		Action: func(c *cli.Context) error {
			// check number of arguments
			if !c.Args().Present() {
				cli.ShowSubcommandHelp(c)
				return errors.New("missing arguments")
			}

			// configure version range resolvers
			if err := vrange.Configure(c); err != nil {
				return err
			}

			// versionsMap contains all versions extracted from
			// licenses export for a given package type and name.
			versionsMap := make(map[string]map[string][]string)

			// load advisories from GLAD
			advisories := []glad.Advisory{}
			for _, path := range c.Args().Slice() {
				fi, err := os.Lstat(path)
				if err != nil {
					return fmt.Errorf("cannot stat '%s': %v", path, err)
				}
				switch mode := fi.Mode(); {
				case mode.IsRegular():
					result, err := glad.ReadAdvisory(path) // TODO read from directories too
					if err != nil {
						return fmt.Errorf("cannot read advisory from '%s': %v", path, err)
					}
					advisories = append(advisories, *result)
				case mode.IsDir():
					result, err := glad.ReadAdvisories(path)
					if err != nil {
						return fmt.Errorf("cannot read advisories from '%s': %v", path, err)
					}
					advisories = append(advisories, result...)
				default:
					return fmt.Errorf("not a regular file or a directory: '%s'", path)
				}
			}

			// initialize versions map
			for _, advisory := range advisories {
				pkgType := advisory.Package.Type
				if _, found := versionsMap[pkgType]; !found {
					versionsMap[pkgType] = make(map[string][]string)
				}
				pkgName := advisory.Package.Name
				if _, found := versionsMap[pkgType][pkgName]; !found {
					versionsMap[pkgType][pkgName] = []string{}
				}
			}

			// cache vrange resolvers for package types to fail early if not found
			resolvers := map[string]vrange.Resolver{}
			for pkgType, _ := range versionsMap {
				resolver, err := vrange.NewResolver(pkgTypeToResolverName(pkgType))
				if err != nil {
					return fmt.Errorf("cannot create resolver for package type '%s': %v", pkgType, err)
				}
				resolvers[pkgType] = resolver
			}

			// collect versions using licenses export
			for pkgType, _ := range versionsMap {
				// list license files
				registry := pkgTypeToRegistry(pkgType)
				pattern := filepath.Join(c.String(flagLicensesV1Path), registry, "**", "*.csv")
				paths, err := filepath.Glob(pattern)
				if err != nil {
					return fmt.Errorf("cannot get license files for '%s': %v", pkgType, err)
				}
				if len(paths) == 0 {
					return fmt.Errorf("no license files for '%s'", pkgType)
				}

				// process files in function in order to safely close them
				processLicenseFile := func(path string) error {
					f, err := os.Open(path)
					if err != nil {
						return fmt.Errorf("cannot open license file '%s': %v", path, err)
					}
					defer f.Close()

					// read lines
					reader := csv.NewReader(f)
					for {
						record, err := reader.Read()
						if err == io.EOF {
							break
						}
						if err != nil {
							return fmt.Errorf("cannot read license file '%s': %v", path, err)
						}
						name, version := record[0], record[1]

						// add version if the package type and name are already in the map
						if versions, found := versionsMap[pkgType][name]; found {
							versionsMap[pkgType][name] = append(versions, version)
						}
					}
					return nil
				}

				// read license files
				for _, path := range paths {
					err := processLicenseFile(path)
					if err != nil {
						return err
					}
				}
			}

			// calculate expectations using vrange and stream them as NDJSON
			encoder := json.NewEncoder(os.Stdout)
			encoder.SetEscapeHTML(false)
			for _, advisory := range advisories {
				pkgType := advisory.Package.Type
				pkgName := advisory.Package.Name
				versions := versionsMap[pkgType][pkgName]

				// skip if no versions in licenses export
				if len(versions) == 0 {
					continue
				}

				// prepare queries
				queries := []vrange.Query{}
				for _, v := range versions {
					q := vrange.Query{
						Range:   advisory.AffectedRange,
						Version: v,
					}
					queries = append(queries, q)
				}

				// resolve queries
				resolver, _ := resolvers[pkgType]
				result, err := resolver.Resolve(queries)
				if err != nil {
					return fmt.Errorf("cannot resolve queries: %v", err)
				}

				// collect results
				versionsIn := []string{}
				versionsOut := []string{}
				versionsErr := []string{}
				for _, q := range queries {
					match, err := result.Satisfies(q)
					if err != nil {
						versionsErr = append(versionsErr, q.Version)
						continue
					}
					if match {
						versionsIn = append(versionsIn, q.Version)
						continue
					}
					versionsOut = append(versionsOut, q.Version)
				}
				exp := Expectation{
					Package: Package{
						Type: pkgType,
						Name: pkgName,
					},
					Versions: Versions{
						In:  versionsIn,
						Out: versionsOut,
						Err: versionsErr,
					},
					VersionRange: advisory.AffectedRange,
				}
				err = encoder.Encode(exp)
				if err != nil {
					return fmt.Errorf("cannot encode expectation to JSON: %v", err)
				}
			}

			return nil
		},
	}
}

// pkgTypeToRegistry converts a GLAD package type to License DB registry name.
func pkgTypeToRegistry(pkgType string) string {
	switch pkgType {
	case "gem":
		return "rubygem"
	default:
		return pkgType
	}
}

// pkgTypeToResolverName converts a GLAD package type to a vrange resolver name.
func pkgTypeToResolverName(pkgType string) string {
	switch pkgType {
	case "npm", "maven", "gem", "go":
		return pkgType
	case "packagist":
		return "php"
	case "pypi":
		return "python"
	default:
		return pkgType
	}
}
